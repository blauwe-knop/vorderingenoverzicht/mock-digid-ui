package events

var (
	MDUI_1 = NewEvent(
		"mdui_1",
		"started listening",
		Low,
	)
	MDUI_2 = NewEvent(
		"mdui_2",
		"server closed",
		High,
	)
	MDUI_3 = NewEvent(
		"mdui_3",
		"received request for home page",
		Low,
	)
	MDUI_4 = NewEvent(
		"mdui_4",
		"sent response with home page",
		Low,
	)
	MDUI_5 = NewEvent(
		"mdui_5",
		"received request for login page",
		Low,
	)
	MDUI_6 = NewEvent(
		"mdui_6",
		"sent response with login page",
		Low,
	)
	MDUI_7 = NewEvent(
		"mdui_7",
		"callback URL missing",
		High,
	)
	MDUI_8 = NewEvent(
		"mdui_8",
		"failed to parse template",
		High,
	)
	MDUI_9 = NewEvent(
		"mdui_9",
		"failed to serve template",
		High,
	)
	MDUI_10 = NewEvent(
		"mdui_10",
		"failed to parse form",
		High,
	)
	MDUI_11 = NewEvent(
		"mdui_11",
		"bsn missing",
		High,
	)
	MDUI_12 = NewEvent(
		"mdui_12",
		"callback URL missing",
		High,
	)
	MDUI_13 = NewEvent(
		"mdui_13",
		"failed to decode callback URL",
		High,
	)
)
