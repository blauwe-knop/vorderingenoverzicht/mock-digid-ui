FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY . /go/src/mock-digid-ui/
WORKDIR /go/src/mock-digid-ui
RUN go mod download \
 && go build -o dist/bin/mock-digid-ui ./cmd/mock-digid-ui

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/mock-digid-ui/dist/bin/mock-digid-ui /usr/local/bin/mock-digid-ui
COPY --from=build /go/src/mock-digid-ui/html/templates/home.html /html/templates/home.html

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/mock-digid-ui"]
