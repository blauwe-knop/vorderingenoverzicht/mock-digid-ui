// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-digid-ui/internal/http_infra/metrics"

	"github.com/go-chi/chi/v5"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"
)

type key int

const (
	htmlTemplateDirectoryKey key = iota
	loggerKey                key = iota
)

type RouterConfig struct {
	HTMLTemplateDirectory string
}

func NewRouter(config RouterConfig, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	metricsMiddleware := metrics.NewMiddleware("mock-digid-ui")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
		ctx := context.WithValue(request.Context(), htmlTemplateDirectoryKey, config.HTMLTemplateDirectory)
		ctx = context.WithValue(ctx, loggerKey, logger)
		handlerHome(responseWriter, request.WithContext(ctx))
	})
	r.Post("/login", func(responseWriter http.ResponseWriter, request *http.Request) {
		ctx := context.WithValue(request.Context(), loggerKey, logger)
		handlerLogin(responseWriter, request.WithContext(ctx))
	})

	healthCheckHandler := healthcheck.NewHandler("mock-digid-ui", []healthcheck.Checker{})
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}
