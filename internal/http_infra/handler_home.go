// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_infra

import (
	"html/template"
	"net/http"
	"path"
	"path/filepath"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-digid-ui/pkg/events"
)

type organization struct {
	CallbackURL string
}

func handlerHome(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	templateDirectory, _ := context.Value(htmlTemplateDirectoryKey).(string)

	event := events.MDUI_3
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	templatePath := filepath.Join(templateDirectory, "home.html")
	filePath := path.Join(templatePath)

	callbackUrlEncoded := request.URL.Query().Get("callbackURL")
	if callbackUrlEncoded == "" {
		event = events.MDUI_7
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "callback URL missing", http.StatusBadRequest)
		return
	}

	t, err := template.ParseFiles(filePath)
	if err != nil {
		event = events.MDUI_8
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	err = t.Execute(responseWriter, &organization{
		CallbackURL: callbackUrlEncoded,
	})
	if err != nil {
		event = events.MDUI_9
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "the page could not be loaded", http.StatusInternalServerError)
		return
	}

	event = events.MDUI_4
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
