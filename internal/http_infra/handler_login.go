// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_infra

import (
	"fmt"
	"net/http"
	"net/url"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-digid-ui/pkg/events"
)

func handlerLogin(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.MDUI_5
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	err := request.ParseForm()
	if err != nil {
		event = events.MDUI_10
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "error parsing form", http.StatusBadRequest)
		return
	}

	bsn := request.Form.Get("bsn")
	if bsn == "" {
		event = events.MDUI_11
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "bsn required", http.StatusBadRequest)
		return
	}

	callBackUrlEncoded := request.Form.Get("callbackURL")
	if callBackUrlEncoded == "" {
		event = events.MDUI_12
		logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		http.Error(responseWriter, "callback URL required", http.StatusBadRequest)
		return
	}

	callbackURLDecoded, err := url.QueryUnescape(callBackUrlEncoded)
	if err != nil {
		event = events.MDUI_13
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "callback URL invalid", http.StatusBadRequest)
		return
	}

	http.Redirect(responseWriter, request, fmt.Sprintf("%s?bsn=%s", callbackURLDecoded, bsn), http.StatusMovedPermanently)

	event = events.MDUI_6
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
