package main

import (
	"errors"
	"fmt"

	"github.com/jessevdk/go-flags"
)

func parseOptions() (options, error) {
	var result options
	args, err := flags.Parse(&result)
	if err != nil {
		var et *flags.Error
		if errors.As(err, &et) {
			if errors.Is(et.Type, flags.ErrHelp) {
				return result, nil
			}
		}
		return result, fmt.Errorf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		return result, fmt.Errorf("unexpected arguments: %v", args)
	}

	return result, err
}
