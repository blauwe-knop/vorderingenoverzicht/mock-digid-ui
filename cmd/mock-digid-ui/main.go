// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-digid-ui/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-digid-ui/pkg/events"
)

type options struct {
	ListenAddress string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the mock-digid-ui api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newZapLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	routerConfig := http_infra.RouterConfig{
		HTMLTemplateDirectory: "html/templates/",
	}

	router := http_infra.NewRouter(routerConfig, logger)

	event := events.MDUI_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))

	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.MDUI_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newZapLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
