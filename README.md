# mock-digid-ui

Procescomponent of participating organizations in the 'Vorderingenoverzicht' system.

## Installation

Prerequisites:

- [Git](https://git-scm.com/)
- [Golang](https://golang.org/doc/install)

1. Download the required Go dependencies:

```sh
go mod download
```

1. Now start the mock digid ui:

```sh
go run cmd/mock-digid-ui/*.go --listen-address 0.0.0.0:8080
```

By default, the application will run on port `80`, so specifically set port 8080 to run it locally.

## Deployment
